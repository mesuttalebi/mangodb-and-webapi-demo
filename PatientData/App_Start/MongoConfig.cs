﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Driver;
using PatientData.Models;

namespace PatientData
{
    public class MongoConfig
    {
        public static async void Seed()
        {
            var patiants = PatientDb.Open();

            var list = await patiants.Find(new BsonDocument("Name", "Mesut")).ToListAsync();

            if (!list.Any())
            {
                var data = new List<Patient>()
                {
                    new Patient() { Name = "Mesut",
                        Medications = new List<Medication>() { new Medication() {Name = "m1", Dozes = 5},new Medication() {Name = "m2", Dozes = 3}},
                        Ailments = new List<Ailment>() { new Ailment() {Name = "A1"}, new Ailment() {Name = "a2"}}
                    },
                    new Patient() { Name = "Cevat",
                        Medications = new List<Medication>() { new Medication() {Name = "m1", Dozes = 2},new Medication() {Name = "m22", Dozes = 2}},
                        Ailments = new List<Ailment>() { new Ailment() {Name = "c1"}, new Ailment() {Name = "ac"}}
                    },
                    new Patient() { Name = "Elif",
                        Medications = new List<Medication>() { new Medication() {Name = "m55", Dozes = 5},new Medication() {Name = "m23", Dozes = 1}},
                        Ailments = new List<Ailment>() { new Ailment() {Name = "A2"}, new Ailment() {Name = "a5"}}
                    }
                };


                await patiants.InsertManyAsync(data);
            }            
        }
    }
}