﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using MongoDB.Bson;
using MongoDB.Driver;
using PatientData.Models;

namespace PatientData.Controllers
{
    [EnableCors("*", "*", "*")]
    public class PatientsController : ApiController
    {
        private readonly IMongoCollection<Patient> _patients;

        public PatientsController()
        {
            _patients = PatientDb.Open();
        }

        public async Task<IEnumerable<Patient>> Get()
        {
            return await _patients.Find(x => true).ToListAsync();
        }

        [HttpGet]
        [Route("api/patients/{id}")]
        public async Task<IHttpActionResult> Get(string Id)
        {
            var patient = await _patients.Find(x=> x.Id == Id).FirstOrDefaultAsync();
            if (patient == null)
            {
                return NotFound();
            }

            return Ok(patient);
        }

        [HttpGet]
        [Route("api/patients/{id}/medications")]
        public async Task<IHttpActionResult> GetMedications(string Id)
        {
            var patient = await _patients.Find(x => x.Id == Id).FirstOrDefaultAsync();
            if (patient == null)
            {
                return NotFound();
            }
            return Ok(patient.Medications);
        }

        [HttpPost]
        [Route("api/patients/update/{id}")]
        public async Task<IHttpActionResult> Update(string id, Patient patient)
        {
            var p = await _patients.ReplaceOneAsync(x => x.Id == id, patient, new UpdateOptions() {IsUpsert = true});
            return Ok(p);
        }

        [HttpPut]
        public async Task<IHttpActionResult> Insert(Patient patient)
        {
            await _patients.InsertOneAsync(patient);
            return Ok("Inserted!");
        }

        [HttpDelete]      
        [Route("api/patients/{id}")]  
        public async Task<IHttpActionResult> DeletePatient(string id)
        {
            var p = await _patients.FindOneAndDeleteAsync(x => x.Id == id);
            return Ok(p);
        }
    }
}
